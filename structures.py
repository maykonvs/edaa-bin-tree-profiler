class BinTreeNode:
    '''
    Nó básico de uma árvore binária.
    '''

    def __init__(self, value: int, parent) -> None:
        self.value = value  # Valor armazenado no nó
        self.parent = parent  # Pai do nó
        self.left = None  # Filho a esquerda
        self.right = None  # Filho a direita
    
    def __lt__(self, other):
        if not other:
            return False

        if isinstance(other, BinTreeNode):
            return self.value < other.value
        
        return self.value < other

    def __gt__(self, other):
        if not other:
            return True
        
        if isinstance(other, BinTreeNode):
            return self.value > other.value
        
        return self.value > other

    def __eq__(self, other):
        if not other:
            return False
        
        if isinstance(other, BinTreeNode):
            return self.value == other.value
        
        return self.value == other


class BinTree:
    '''
    Classe base para as implementações de árvores binárias.
    '''

    name = ""  # Nome da estrutura da árvore

    def __init__(self) -> None:
        '''
        Inicializa uma árvore vazia.
        '''
        self.nil = self._create_nil_node()
        self.root = self.nil  # Raiz da árvore
        self.len = 0  # Número de elementos
        self.add_comp_count = 0  # Contador de comparações totais na construção da árvore
        self.find_comp_count = 0  # Contador de comparações totais nas consultas a árvore

    def add(self, value: int) -> BinTreeNode:
        '''
        Adiciona um item à árvore e retorna nó criado.
        Retorna None se o elemento não foi adicionado.
        '''

        # Se a árvore está vazia nós acrescentamos o novo elemento à raiz.
        if self.root == self.nil:
            new_node = self._create_node(value, None)
            self.root = new_node
            self.len = 1
            return new_node
        
        # Do contrário nós delegamos para um função genérica, que deve ser
        # implementada por classes mais específicas, que irá então acrescentar
        # o elemento à árvore.
        new_node = self._add(self.root, value)

        if new_node:  # Verificando se o elemento foi adicionando
            self.len += 1  # Incrementando número de elementos da árvore.

        return new_node
        
    
    def find(self, value: int) -> BinTreeNode:
        '''
        Busca por um elemento na árvore e retorna o nó que o contém.
        Retorna None se o elemento não foi encontrado.
        '''
        return self._find(self.root, value)
    
    def _create_node(self, value: int, parent: BinTreeNode) -> BinTreeNode:
        '''
        Cria um nó vazio, contendo apenas o valor passado por parâmetro.
        '''
        node = BinTreeNode(value, parent)
        node.left = self.nil
        node.right = self.nil
        return node
    
    def _create_nil_node(self):
        '''
        Cria o nó que representa o nulo na árvore.
        '''
        return BinTreeNode(0, None)

    def _add(self, node: BinTreeNode, value: int) -> BinTreeNode:
        '''
        Adiciona um elemento à árvore não vazia e retorna o nó criado.
        Retorna None se o elemento não foi adicionado.
        '''
        raise NotImplementedError()

    def _find(self, node: BinTreeNode, value: int) -> BinTreeNode:
        '''
        Percorre a árvore, iniciando no nó passado por parâmetro, 
        procurando pelo valor.
        '''
        raise NotImplementedError()

    def __len__(self):
        '''
        Número de elementos da árvore.
        '''
        return self.len


class IterativeBinSearchTree(BinTree):
    '''
    Árvore binária de busca. Especificação das árvores binárias.
    '''
    
    name = "Árvore Binária de Busca Iterativa"

    def _add(self, node: BinTreeNode, value: int) -> BinTreeNode:
        parent_node = None
        curr_node = node

        while curr_node != self.nil:
            self.add_comp_count += 1

            parent_node = curr_node

            if value < curr_node:
                curr_node = curr_node.left
            elif value > curr_node:
                curr_node = curr_node.right
            else:
                return None

        new_node = self._create_node(value, parent_node)

        if value < parent_node:
            parent_node.left = new_node
        else:
            parent_node.right = new_node

        return new_node

    def _find(self, node: BinTreeNode, value: int) -> BinTreeNode:
        curr_node = node

        while curr_node != self.nil:
            self.find_comp_count += 1

            if value == curr_node:
                return curr_node
            
            if value < curr_node:
                curr_node = curr_node.left
            else:
                curr_node = curr_node.right
            
        return None


class RecursiveBinSearchTree(BinTree):
    '''
    Árvore binária de busca. Especificação das árvores binárias.
    '''
    
    name = "Árvore Binária de Busca Recursiva"

    def _add(self, node: BinTreeNode, value: int) -> BinTreeNode:
        self.add_comp_count += 1

        if value < node:  # Verificando se o valor a ser acrescentando é menor que o do nó
            # Se for, nós verificamos se o nó tem um filho a esquerda
            if node.left == self.nil:
                # Se não tem, nós criamos um novo nó e colocamos ele como filho a esquerda do
                # nó atual.
                new_node = self._create_node(value, node)
                node.left = new_node
                return new_node
            
            # Se tem, nós passamos a analisar o valor em relação ao filho a esquerda.
            return self._add(node.left, value)
        
        if value > node:  # Verificando se o valor a ser acrescentado é maior o do nó
            # Se for, nós verificamos se o nó tem um filho a direita
            if node.right == self.nil:
                # Se não tem, nós criamos um novo nó e colocamos ele ocmo filho a direita do 
                # nó atual.
                new_node = self._create_node(value, node)
                node.right = new_node
                return new_node
            
            # Se tem, nós passamos a analisar o valor em relação ao filho a direita
            return self._add(node.right, value)

        # Se o valor for igual, nós não acrescentamos ele
        return None

    def _find(self, node: BinTreeNode, value: int) -> BinTreeNode:
        self.find_comp_count += 1

        if not node:
            return None

        if value == node:
            return node

        if value < node:
            return self._find(node.left, value)

        return self._find(node.right, value)


class RBBinTreeNode(BinTreeNode):

    COLOR_RED = False
    COLOR_BLACK = True

    def __init__(self, value: int, parent) -> None:
        super().__init__(value, parent)
        self.color = RBBinTreeNode.COLOR_RED


class RBBinTreeMixin:

    def add(self, value: int) -> BinTreeNode:
        node = super().add(value)

        if node:
            self._add_fixup(node)

        return node

    def _create_node(self, value: int, parent: RBBinTreeNode) -> RBBinTreeNode:
        node = RBBinTreeNode(value, parent)
        node.left = self.nil
        node.right = self.nil
        return node

    def _create_nil_node(self):
        node = RBBinTreeNode(0, None)
        node.color = RBBinTreeNode.COLOR_BLACK
        return node
    
    def _rotate_left(self, node: RBBinTreeNode):
        y = node.right
        node.right = y.left
        if y.left != self.nil:
            y.left.parent = node

        y.parent = node.parent
        if node.parent is None:
            self.root = y
        elif node == node.parent.left:
            node.parent.left = y
        else:
            node.parent.right = y
        
        y.left = node
        node.parent = y

    def _rotate_right(self, node: RBBinTreeNode):
        y = node.left
        node.left = y.right
        if y.right != self.nil:
            y.right.parent = node
            
        y.parent = node.parent
        if node.parent is None:
            self.root = y
        elif node == node.parent.right:
            node.parent.right = y
        else:
            node.parent.left = y
        
        y.right = node
        node.parent = y

    def _add_fixup(self, node:RBBinTreeNode):
        while node != self.root and node.parent.color == RBBinTreeNode.COLOR_RED:
            if node.parent == node.parent.parent.right:
                u = node.parent.parent.left  # uncle
                if u.color == RBBinTreeNode.COLOR_RED:
                    u.color = RBBinTreeNode.COLOR_BLACK
                    node.parent.color = RBBinTreeNode.COLOR_BLACK
                    node.parent.parent.color = RBBinTreeNode.COLOR_RED
                    node = node.parent.parent
                else:
                    if node == node.parent.left:
                        node = node.parent
                        self._rotate_right(node)
                    node.parent.color = RBBinTreeNode.COLOR_BLACK
                    node.parent.parent.color = RBBinTreeNode.COLOR_RED
                    self._rotate_left(node.parent.parent)
            else:
                u = node.parent.parent.right  # uncle

                if u.color == RBBinTreeNode.COLOR_RED:
                    u.color = RBBinTreeNode.COLOR_BLACK
                    node.parent.color = RBBinTreeNode.COLOR_BLACK
                    node.parent.parent.color = RBBinTreeNode.COLOR_RED
                    node = node.parent.parent
                else:
                    if node == node.parent.right:
                        node = node.parent
                        self._rotate_left(node)
                    node.parent.color = RBBinTreeNode.COLOR_BLACK
                    node.parent.parent.color = RBBinTreeNode.COLOR_RED
                    self._rotate_right(node.parent.parent)
        self.root.color = RBBinTreeNode.COLOR_BLACK


class IterativeRBBinTree(RBBinTreeMixin, IterativeBinSearchTree):
    name = "Árvore Rubro Negra Iterativa"

class RecursiveRBBinTree(RBBinTreeMixin, RecursiveBinSearchTree):
    name = "Árvore Rubro Negra Recursiva"

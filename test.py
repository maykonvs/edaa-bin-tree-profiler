import unittest

import structures as st
import profile as pf


class BinTreeNodeTest(unittest.TestCase):

    def test_comp(self):
        node = st.BinTreeNode(5, None)

        self.assertLess(node, 6)
        self.assertGreater(node, 4)


class BinSearchTreeTest(unittest.TestCase):

    def _add_test(self, tree):
        node_0 = tree.add(5)

        self.assertIsNotNone(node_0)
        self.assertEqual(node_0.value, 5)
        self.assertIs(tree.root, node_0)
        self.assertIsNone(node_0.parent)
        self.assertEqual(len(tree), 1)

        node_1 = tree.add(3)

        self.assertIsNotNone(node_1)
        self.assertEqual(node_1.value, 3)
        self.assertIs(node_0.left, node_1)
        self.assertIs(node_1.parent, node_0)
        self.assertEqual(len(tree), 2)

        node_2 = tree.add(7)

        self.assertIsNotNone(node_2)
        self.assertEqual(node_2.value, 7)
        self.assertIs(node_0.right, node_2)
        self.assertIs(node_2.parent, node_0)
        self.assertEqual(len(tree), 3)

        node_3 = tree.add(2)

        self.assertIsNotNone(node_3)
        self.assertEqual(node_3.value, 2)
        self.assertIs(node_1.left, node_3)
        self.assertIs(node_3.parent, node_1)
        self.assertEqual(len(tree), 4)

        node_4 = tree.add(4)

        self.assertIsNotNone(node_4)
        self.assertEqual(node_4.value, 4)
        self.assertIs(node_1.right, node_4)
        self.assertIs(node_4.parent, node_1)
        self.assertEqual(len(tree), 5)

        node_5 = tree.add(8)

        self.assertIsNotNone(node_5)
        self.assertEqual(node_5.value, 8)
        self.assertIs(node_2.right, node_5)
        self.assertIs(node_5.parent, node_2)
        self.assertEqual(len(tree), 6)

        node_6 = tree.add(6)

        self.assertIsNotNone(node_6)
        self.assertEqual(node_6.value, 6)
        self.assertIs(node_2.left, node_6)
        self.assertIs(node_6.parent, node_2)
        self.assertEqual(len(tree), 7)

        node_7 = tree.add(2)

        self.assertIsNone(node_7)
        self.assertEqual(len(tree), 7)

    def _find_test(self, tree):
        tree.add(5)
        tree.add(7)
        tree.add(4)
        tree.add(2)
        tree.add(9)
        tree.add(10)

        node = tree.find(5)

        self.assertEqual(node.value, 5)

        node = tree.find(15)

        self.assertIsNone(node)

    def test_add_iterative(self):
        self._add_test(st.IterativeBinSearchTree())

    def test_add_recursive(self):
        self._add_test(st.RecursiveBinSearchTree())

    def test_find_iterative(self):
        self._find_test(st.IterativeBinSearchTree())

    def test_find_recursive(self):
        self._find_test(st.RecursiveBinSearchTree())


class RBBinTreeTest(unittest.TestCase):

    def _add_test(self, tree):
        node_3 = tree.add(3)

        self.assertIsNotNone(node_3)
        self.assertEqual(node_3.value, 3)
        self.assertIs(tree.root, node_3)
        self.assertIsNone(node_3.parent)
        self.assertEqual(node_3.color, st.RBBinTreeNode.COLOR_BLACK)
        self.assertEqual(len(tree), 1)

        node_1 = tree.add(1)

        self.assertIsNotNone(node_1)
        self.assertEqual(node_1.value, 1)
        self.assertIs(tree.root, node_3)
        self.assertIs(node_3.left, node_1)
        self.assertIsNone(node_3.parent)
        self.assertIs(node_1.parent, node_3)
        self.assertEqual(node_3.color, st.RBBinTreeNode.COLOR_BLACK)
        self.assertEqual(node_1.color, st.RBBinTreeNode.COLOR_RED)
        self.assertEqual(len(tree), 2)

        node_2 = tree.add(2)

        self.assertIsNotNone(node_2)
        self.assertEqual(node_2.value, 2)
        self.assertIs(tree.root, node_2)
        self.assertIsNone(node_2.parent)
        self.assertIs(node_2.left, node_1)
        self.assertIs(node_2.right, node_3)
        self.assertIs(node_1.parent, node_2)
        self.assertIs(node_3.parent, node_2)
        self.assertEqual(node_2.color, st.RBBinTreeNode.COLOR_BLACK)
        self.assertEqual(node_1.color, st.RBBinTreeNode.COLOR_RED)
        self.assertEqual(node_3.color, st.RBBinTreeNode.COLOR_RED)
        self.assertEqual(len(tree), 3)

        node_7 = tree.add(7)

        self.assertIsNotNone(node_7)
        self.assertEqual(node_7.value, 7)
        self.assertIs(tree.root, node_2)
        self.assertIs(node_2.left, node_1)
        self.assertIs(node_2.right, node_3)
        self.assertIs(node_3.right, node_7)
        self.assertIsNone(node_2.parent)
        self.assertIs(node_1.parent, node_2)
        self.assertIs(node_3.parent, node_2)
        self.assertIs(node_7.parent, node_3)
        self.assertEqual(node_2.color, st.RBBinTreeNode.COLOR_BLACK)
        self.assertEqual(node_1.color, st.RBBinTreeNode.COLOR_BLACK)
        self.assertEqual(node_3.color, st.RBBinTreeNode.COLOR_BLACK)
        self.assertEqual(node_7.color, st.RBBinTreeNode.COLOR_RED)
        self.assertEqual(len(tree), 4)

        node_9 = tree.add(9)

        self.assertIsNotNone(node_9)
        self.assertEqual(node_9.value, 9)
        self.assertIs(tree.root, node_2)
        self.assertIs(node_2.left, node_1)
        self.assertIs(node_2.right, node_7)
        self.assertIs(node_7.left, node_3)
        self.assertIs(node_7.right, node_9)
        self.assertIsNone(node_2.parent)
        self.assertIs(node_1.parent, node_2)
        self.assertIs(node_7.parent, node_2)
        self.assertIs(node_3.parent, node_7)
        self.assertIs(node_9.parent, node_7)
        self.assertEqual(node_2.color, st.RBBinTreeNode.COLOR_BLACK)
        self.assertEqual(node_1.color, st.RBBinTreeNode.COLOR_BLACK)
        self.assertEqual(node_7.color, st.RBBinTreeNode.COLOR_BLACK)
        self.assertEqual(node_3.color, st.RBBinTreeNode.COLOR_RED)
        self.assertEqual(node_9.color, st.RBBinTreeNode.COLOR_RED)
        self.assertEqual(len(tree), 5)

        node_5 = tree.add(5)

        self.assertIsNotNone(node_5)
        self.assertEqual(node_5.value, 5)
        self.assertIs(tree.root, node_2)
        self.assertIs(node_2.left, node_1)
        self.assertIs(node_2.right, node_7)
        self.assertIs(node_7.left, node_3)
        self.assertIs(node_7.right, node_9)
        self.assertIs(node_3.right, node_5)
        self.assertIsNone(node_2.parent)
        self.assertIs(node_1.parent, node_2)
        self.assertIs(node_7.parent, node_2)
        self.assertIs(node_3.parent, node_7)
        self.assertIs(node_9.parent, node_7)
        self.assertIs(node_5.parent, node_3)
        self.assertEqual(node_2.color, st.RBBinTreeNode.COLOR_BLACK)
        self.assertEqual(node_1.color, st.RBBinTreeNode.COLOR_BLACK)
        self.assertEqual(node_7.color, st.RBBinTreeNode.COLOR_RED)
        self.assertEqual(node_3.color, st.RBBinTreeNode.COLOR_BLACK)
        self.assertEqual(node_9.color, st.RBBinTreeNode.COLOR_BLACK)
        self.assertEqual(node_5.color, st.RBBinTreeNode.COLOR_RED)
        self.assertEqual(len(tree), 6)

        node_4 = tree.add(4)

        self.assertIsNotNone(node_4)
        self.assertEqual(node_4.value, 4)
        self.assertIs(tree.root, node_2)
        self.assertIs(node_2.left, node_1)
        self.assertIs(node_2.right, node_7)
        self.assertIs(node_7.left, node_4)
        self.assertIs(node_7.right, node_9)
        self.assertIs(node_4.left, node_3)
        self.assertIs(node_4.right, node_5)
        self.assertIsNone(node_2.parent)
        self.assertIs(node_1.parent, node_2)
        self.assertIs(node_7.parent, node_2)
        self.assertIs(node_4.parent, node_7)
        self.assertIs(node_9.parent, node_7)
        self.assertIs(node_3.parent, node_4)
        self.assertIs(node_5.parent, node_4)
        self.assertEqual(node_2.color, st.RBBinTreeNode.COLOR_BLACK)
        self.assertEqual(node_1.color, st.RBBinTreeNode.COLOR_BLACK)
        self.assertEqual(node_7.color, st.RBBinTreeNode.COLOR_RED)
        self.assertEqual(node_4.color, st.RBBinTreeNode.COLOR_BLACK)
        self.assertEqual(node_9.color, st.RBBinTreeNode.COLOR_BLACK)
        self.assertEqual(node_3.color, st.RBBinTreeNode.COLOR_RED)
        self.assertEqual(node_5.color, st.RBBinTreeNode.COLOR_RED)
        self.assertEqual(len(tree), 7)

        node_6 = tree.add(6)

        self.assertIsNotNone(node_6)
        self.assertEqual(node_6.value, 6)
        self.assertIs(tree.root, node_4)
        self.assertIs(node_4.left, node_2)
        self.assertIs(node_4.right, node_7)
        self.assertIs(node_2.left, node_1)
        self.assertIs(node_2.right, node_3)
        self.assertIs(node_7.left, node_5)
        self.assertIs(node_7.right, node_9)
        self.assertIs(node_5.right, node_6)
        self.assertIsNone(node_4.parent)
        self.assertIs(node_2.parent, node_4)
        self.assertIs(node_7.parent, node_4)
        self.assertIs(node_1.parent, node_2)
        self.assertIs(node_3.parent, node_2)
        self.assertIs(node_5.parent, node_7)
        self.assertIs(node_9.parent, node_7)
        self.assertIs(node_6.parent, node_5)
        self.assertEqual(node_4.color, st.RBBinTreeNode.COLOR_BLACK)
        self.assertEqual(node_2.color, st.RBBinTreeNode.COLOR_RED)
        self.assertEqual(node_7.color, st.RBBinTreeNode.COLOR_RED)
        self.assertEqual(node_1.color, st.RBBinTreeNode.COLOR_BLACK)
        self.assertEqual(node_3.color, st.RBBinTreeNode.COLOR_BLACK)
        self.assertEqual(node_5.color, st.RBBinTreeNode.COLOR_BLACK)
        self.assertEqual(node_9.color, st.RBBinTreeNode.COLOR_BLACK)
        self.assertEqual(node_6.color, st.RBBinTreeNode.COLOR_RED)
        self.assertEqual(len(tree), 8)

        node_8 = tree.add(8)

        self.assertIsNotNone(node_8)
        self.assertEqual(node_8.value, 8)
        self.assertIs(tree.root, node_4)
        self.assertIs(node_4.left, node_2)
        self.assertIs(node_4.right, node_7)
        self.assertIs(node_2.left, node_1)
        self.assertIs(node_2.right, node_3)
        self.assertIs(node_7.left, node_5)
        self.assertIs(node_7.right, node_9)
        self.assertIs(node_5.right, node_6)
        self.assertIs(node_9.left, node_8)
        self.assertIsNone(node_4.parent)
        self.assertIs(node_2.parent, node_4)
        self.assertIs(node_7.parent, node_4)
        self.assertIs(node_1.parent, node_2)
        self.assertIs(node_3.parent, node_2)
        self.assertIs(node_5.parent, node_7)
        self.assertIs(node_9.parent, node_7)
        self.assertIs(node_6.parent, node_5)
        self.assertIs(node_8.parent, node_9)
        self.assertEqual(node_4.color, st.RBBinTreeNode.COLOR_BLACK)
        self.assertEqual(node_2.color, st.RBBinTreeNode.COLOR_RED)
        self.assertEqual(node_7.color, st.RBBinTreeNode.COLOR_RED)
        self.assertEqual(node_1.color, st.RBBinTreeNode.COLOR_BLACK)
        self.assertEqual(node_3.color, st.RBBinTreeNode.COLOR_BLACK)
        self.assertEqual(node_5.color, st.RBBinTreeNode.COLOR_BLACK)
        self.assertEqual(node_9.color, st.RBBinTreeNode.COLOR_BLACK)
        self.assertEqual(node_6.color, st.RBBinTreeNode.COLOR_RED)
        self.assertEqual(node_8.color, st.RBBinTreeNode.COLOR_RED)
        self.assertEqual(len(tree), 9)


    def _find_test(self, tree):
        tree.add(5)
        tree.add(7)
        tree.add(4)
        tree.add(2)
        tree.add(9)
        tree.add(10)

        node = tree.find(5)

        self.assertEqual(node.value, 5)

        node = tree.find(15)

        self.assertIsNone(node)
    
    def test_add_iterative(self):
        self._add_test(st.IterativeRBBinTree())
    
    def test_add_recursive(self):
        self._add_test(st.RecursiveRBBinTree())
    
    def test_find_iterative(self):
        self._find_test(st.IterativeRBBinTree())
    
    def test_find_recursive(self):
        self._find_test(st.RecursiveRBBinTree())


class ProfileTestTest(unittest.TestCase):

    def _bin_search_tree_comp_count_test(self, tree_class):
        profile = pf.Profile()
        profile.tree = tree_class()
        profile.construct_data = [1]
        profile.search_data = [1]
        result = profile.run()

        self.assertEqual(result.construct_comp_count, 0)
        self.assertEqual(result.search_comp_count, 1)

        profile.tree = tree_class()
        profile.construct_data = [1, 3]
        profile.search_data = [3]
        result = profile.run()
        
        self.assertEqual(result.construct_comp_count, 1)
        self.assertEqual(result.search_comp_count, 2)

        profile.tree = tree_class()
        profile.construct_data = [1, 3, 2]
        profile.search_data = [3, 2]
        result = profile.run()
        
        self.assertEqual(result.construct_comp_count, 3)
        self.assertEqual(result.search_comp_count, 5)

        profile.tree = tree_class()
        profile.construct_data = [1, 3, 2, 4]
        profile.search_data = [2, 4]
        result = profile.run()
        
        self.assertEqual(result.construct_comp_count, 5)
        self.assertEqual(result.search_comp_count, 6)

        profile.tree = tree_class()
        profile.construct_data = [1, 3, 2, 4, 7]
        profile.search_data = [3, 4, 7]
        result = profile.run()
        
        self.assertEqual(result.construct_comp_count, 8)
        self.assertEqual(result.search_comp_count, 9)

        profile.tree = tree_class()
        profile.construct_data = [1, 3, 2, 4, 7, 6]
        profile.search_data = [4, 7, 6]
        result = profile.run()
        
        self.assertEqual(result.construct_comp_count, 12)
        self.assertEqual(result.search_comp_count, 12)

    def _rb_bin_tree_construct_comp_count_test(self, tree_class):
        profile = pf.Profile()
        profile.tree = tree_class()
        profile.construct_data = [1]
        profile.search_data = [1]
        result = profile.run()

        self.assertEqual(result.construct_comp_count, 0)
        self.assertEqual(result.search_comp_count, 1)

        profile.tree = tree_class()
        profile.construct_data = [1, 3]
        profile.search_data = [3]
        result = profile.run()
        
        self.assertEqual(result.construct_comp_count, 1)
        self.assertEqual(result.search_comp_count, 2)

        profile.tree = tree_class()
        profile.construct_data = [1, 3, 2]
        profile.search_data = [3, 2]
        result = profile.run()
        
        self.assertEqual(result.construct_comp_count, 3)
        self.assertEqual(result.search_comp_count, 3)

        profile.tree = tree_class()
        profile.construct_data = [1, 3, 2, 4]
        profile.search_data = [2, 4]
        result = profile.run()
        
        self.assertEqual(result.construct_comp_count, 5)
        self.assertEqual(result.search_comp_count, 4)

        profile.tree = tree_class()
        profile.construct_data = [1, 3, 2, 4, 7]
        profile.search_data = [3, 4, 7]
        result = profile.run()
        
        self.assertEqual(result.construct_comp_count, 8)
        self.assertEqual(result.search_comp_count, 8)

        profile.tree = tree_class()
        profile.construct_data = [1, 3, 2, 4, 7, 6]
        profile.search_data = [4, 7, 6]
        result = profile.run()
        
        self.assertEqual(result.construct_comp_count, 11)
        self.assertEqual(result.search_comp_count, 9)
    
    def test_iterative_bin_search_tree_comp_count(self):
        self._bin_search_tree_comp_count_test(st.IterativeBinSearchTree)
    
    def test_recursive_bin_search_tree_comp_count(self):
        self._bin_search_tree_comp_count_test(st.RecursiveBinSearchTree)
    
    def test_iterative_rb_bin_tree_comp_count(self):
        self._rb_bin_tree_construct_comp_count_test(st.IterativeRBBinTree)
    
    def test_recursive_rb_bin_tree_comp_count(self):
        self._rb_bin_tree_construct_comp_count_test(st.RecursiveRBBinTree)
        

if __name__ == '__main__':
    unittest.main()

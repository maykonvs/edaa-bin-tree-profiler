# Introdução

Projeto criado para um trabalho da displina de Estrutura de Dados e Análise de Algoritmos do Mestrado em Ciência da Computação da Unioeste, Professor André Luiz Brun.

## Requisitos

- Python 3.9.6*

**\* O projeto foi criado e testado no Python 3.9.6, portanto é garantido que roda nesta versão, porém provavelmente ele rode em qualquer versão 3.\*.\* do python.**

# Especificação do Trabalho

## Objetivo

Aplicar e corroborar conceitos adquiridos com relação aos métodos de balanceamento de árvores e seu impacto no custo de sua manipulação.

## Tarefa

Implementar as seguintes estratégias de construção de árvores binárias:
1. Árvore Binária de Busca;
1. Árvore Rubro Negra.

Avaliar o comportamento dos métodos perante um conjunto de testes com diferentes características.

## Como

- A linguagem utilizada no desenvolvimento é de sua escolha.
- A forma com que os métodos serão implementados é determinada pelo(a) aluno(a).
- A entrada dos dados para a **construção** das árvores deve ser feita com base nos arquivos texto disponíveis neste [link](https://drive.google.com/file/d/1yWOHj9HRoNcHXa5WAZpAhtAX2SqdqO7L/view).
- Os valores a serem utilizados no momento da **consulta** estão disponíveis neste [link](https://drive.google.com/file/d/1zQNwOhjlY9PQX4y7rjmC24VZm7WERhYN/view).
- Critérios que devem ser analisados:
    - Durante a **construção** das árvores:
        - Tempo cronológico gasto na construção das estruturas;
        - Número de comparações entre chaves realizadas para construir cada uma das estruturas;
    - Durante a **consulta** às arvores:
        - Tempo cronológico gasto na consulta de **todos os elementos** presentes no arquivo dentro das duas estruturas construídas anteriormente;
        - Número de comparações realizadas entre chaves para consultar **todos os elementos** dos arquivos de consulta nas árvores formadas anteriormente.

from posixpath import basename
import profile as pf
import serializers as sl
import structures as st


class Runner:

    def _get_data(self, file):
        with open(file, "r") as file:
            data = list()

            for line in file:
                for value in line.split():
                    data.append(int(value))

            return data

    def run(self, id, name, construct_file, search_file, tree_class):
        profile = pf.Profile()
        profile.tree = tree_class()
        profile.construct_data = self._get_data(construct_file)
        profile.search_data = self._get_data(search_file)

        print()
        print("EXECUTANDO TESTE {0} - {1}".format(id, name))
        print()

        result = profile.run()

        result.id = id

        print("Tempo de Construção: {0}".format(result.construct_execution_time))
        print("Comparações na Construção: {0}".format(result.construct_comp_count))
        print("Tempo de Consulta: {0}".format(result.search_execution_time))
        print("Comparações na Consulta: {0}".format(result.search_comp_count))
        print()
        print("TESTE FINALIZADO ")
        print()

        return result


if __name__ == '__main__':
    import glob
    import os.path

    runner = Runner()
    results = list()

    print("INICIANDO TESTES -----------------------------")

    for i in range(1, 6):
        for construct_file in glob.glob("data/construir/*.txt"):
            file_name = os.path.basename(construct_file)
            search_file = os.path.join("data/consultar", file_name)
            if os.path.isfile(search_file):
                name = os.path.splitext(file_name)[0]

                l = int(name)

                result = runner.run(i, name, construct_file, search_file, st.IterativeBinSearchTree)
                results.append(result)
                result = runner.run(i, name, construct_file, search_file, st.IterativeRBBinTree)
                results.append(result)

                if l <= 30000:
                    result = runner.run(i, name, construct_file, search_file, st.RecursiveBinSearchTree)
                    results.append(result)
                    result = runner.run(i, name, construct_file, search_file, st.RecursiveRBBinTree)
                    results.append(result)
    
    print("TESTES FINALIZADOS ---------------------------")
    
    serializer = sl.CSVSerializer()
    serializer.serialize(results, "output.csv")

from time import perf_counter_ns


class ProfileResult:
    '''
    Contém os resultados de um profile que consistirá de diversos TestResults.
    '''

    def __init__(self) -> None:
        self.id = 0
        self.tree_name = ''
        self.profile_size = 0
        self.construct_comp_count = 0
        self.construct_execution_time = 0
        self.search_comp_count = 0
        self.search_execution_time = 0


class Profile:
    '''
    Representa um benchmark de construção e pesquisa em uma estrutura de árvore binária.
    '''

    def __init__(self) -> None:
        self.tree = None
        self.construct_data = None
        self.search_data = None
    
    def run(self) -> ProfileResult:
        context = ProfileResult()
        context.tree_name = self.tree.name
        context.profile_size = len(self.construct_data)

        start = perf_counter_ns()

        for value in self.construct_data:
            self.tree.add(value)
            # node = self.tree.add(value)
            # if node:
            #     print("+ [{0}] - {1} - {2}".format(len(self.tree), value, node.value))
            # else:
            #     print("+ [{0}] - {1} - REPETIDO".format(len(self.tree), value))

        end = perf_counter_ns()

        context.construct_comp_count = self.tree.add_comp_count
        context.construct_execution_time = end - start


        if self.search_data:
            start = perf_counter_ns()

            for value in self.search_data:
                self.tree.find(value)
                # node = self.tree.find(value)
                # if node:
                #     print("* [{0}] - {1} - {2}".format(len(self.tree), value, node.value))
                # else:
                #     print("* [{0}] - {1} - NÃO ENCONTRADO".format(len(self.tree), value))

            end = perf_counter_ns()

            context.search_comp_count = self.tree.find_comp_count
            context.search_execution_time = end - start

        return context
